#!/bin/bash

# Функция для проверки наличия команды и её установки
check_and_install() {
    if ! command -v "$1" &>/dev/null; then
        echo "Команда $1 не найдена. Установка..."
        sudo apt-get install -y "$1"
    fi
}

# Проверка и установка необходимых команд
check_and_install wget
check_and_install id3v2
check_and_install mp3val
check_and_install ffmpeg

# Инициализация массива для хранения имен файлов
declare -a FILES

# Форматирование даты и времени для названия
DT_FORMAT=$(date +"%Y-%m-%d %H:%M")

# Функция для получения текущего времени и форматирования его в нужный формат
get_filename() {
    echo $(date +"%Y-%m-%d-%H-%M-%S")-dharma1937fm.mp3
}

# Добавление ID3 тегов к выходному файлу
add_tags() {
    TAG_TITLE="Эфир dharma1937.fm от $DT_FORMAT"
    TAG_ARTIST="dharma1937.fm"

    # Обновление тегов
    id3v2 -t "$TAG_TITLE" -a "$TAG_ARTIST" "$1"
}

# Функция для обработки сигнала прерывания (SIGINT)
cleanup() {
    echo "Прерывание скрипта. Запуск mp3val..."
    for file in "${FILES[@]}"; do
        mp3val "$file" -f -t
    done

    if [ "${#FILES[@]}" -eq 1 ]; then
        # Если есть только один файл
        add_tags "${FILES[0]}"
    elif [ "${#FILES[@]}" -gt 1 ]; then
        echo "Склеивание файлов..."

        # Определение названия выходного файла
        OUTPUT_FILE="${FILES[0]}-output.mp3"

        # Создание временного файла со списком всех аудиофайлов
        FILELIST="filelist.txt"
        >"$FILELIST"
        for file in "${FILES[@]}"; do
            echo "file '$file'" >>"$FILELIST"
        done

        # Склеивание файлов с помощью ffmpeg и сохранение с новым именем
        ffmpeg -f concat -safe 0 -i "$FILELIST" -c copy "$OUTPUT_FILE"
        rm "$FILELIST"

        # Добавление ID3 тегов
        add_tags "$OUTPUT_FILE"
    fi

    exit
}

# Установка обработчика сигнала SIGINT
trap cleanup SIGINT

# Бесконечный цикл для перезапуска записи
while true; do
    # Генерация имени файла
    FILENAME=$(get_filename)
    # Добавление имени файла в массив
    FILES+=("$FILENAME")

    # Выполнение команды wget для скачивания аудиопотока
    wget -O "$FILENAME" http://second.dharma1937.fm:7777/live
    # Краткая пауза перед следующей попыткой записи (на случай, если поток не доступен)
    sleep 5
done
