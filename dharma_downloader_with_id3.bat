# Функция для проверки наличия команды и её установки
function Check-And-Install($command) {
    $path = Get-Command $command -ErrorAction SilentlyContinue
    if ($null -eq $path) {
        Write-Host "Команда $command не найдена. Установите её вручную."
        # Здесь может быть код для автоматической установки, если возможно
    }
}

# Проверка и установка необходимых команд (предполагается, что они установлены вручную)
Check-And-Install 'id3v2'
Check-And-Install 'mp3val'
Check-And-Install 'ffmpeg'

# Инициализация массива для хранения имен файлов
$FILES = @()

# Функция для получения текущего времени и форматирования его в нужный формат
function Get-Filename {
    return Get-Date -Format "yyyy-MM-dd-HH-mm-ss-dharma1937fm.mp3"
}

# Добавление ID3 тегов к выходному файлу
function Add-Tags($file) {
    $DT_FORMAT = Get-Date -Format "yyyy-MM-dd HH:mm"
    $TAG_TITLE = "Эфир dharma1937.fm от $DT_FORMAT"
    $TAG_ARTIST = "dharma1937.fm"

    # Обновление тегов
    id3v2 -t $TAG_TITLE -a $TAG_ARTIST $file
}

# Функция для обработки сигнала прерывания (Ctrl+C)
function Cleanup {
    Write-Host "Прерывание скрипта. Запуск mp3val..."
    foreach ($file in $FILES) {
        mp3val $file -f -t
    }

    if ($FILES.Count -eq 1) {
        # Если есть только один файл
        Add-Tags $FILES[0]
    } elseif ($FILES.Count -gt 1) {
        Write-Host "Склеивание файлов..."

        # Определение названия выходного файла
        $OUTPUT_FILE = $FILES[0] + "-output.mp3"

        # Создание временного файла со списком всех аудиофайлов
        $FILELIST = "filelist.txt"
        "" > $FILELIST
        foreach ($file in $FILES) {
            Add-Content $FILELIST "file '$file'"
        }

        # Склеивание файлов с помощью ffmpeg и сохранение с новым именем
        ffmpeg -f concat -safe 0 -i $FILELIST -c copy $OUTPUT_FILE
        Remove-Item $FILELIST

        # Добавление ID3 тегов
        Add-Tags $OUTPUT_FILE
    }

    exit
}

# Регистрация обработчика сигнала прерывания (Ctrl+C)
$Host.UI.RawUI.KeyAvailableAction = { Cleanup }

# Бесконечный цикл для перезапуска записи
while ($true) {
    # Генерация имени файла
    $FILENAME = Get-Filename
    # Добавление имени файла в массив
    $FILES += $FILENAME

    # Выполнение команды Invoke-WebRequest для скачивания аудиопотока
    Invoke-WebRequest -Uri "http://second.dharma1937.fm:7777/live" -OutFile $FILENAME
    # Краткая пауза перед следующей попыткой записи
    Start-Sleep -Seconds 5
}
