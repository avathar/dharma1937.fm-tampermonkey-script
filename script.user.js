// ==UserScript==
// @name         dharma1937.fm
// @namespace    http://tampermonkey.net/
// @version      0.1.16
// @description  try to take over the world!
// @author       avathar aka mrAvathar <pperov@gmail.com>
// @match        http://dharma1937.fm/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=dharma1937.fm
// @grant        none
// @downloadURL  https://gitlab.com/avathar/dharma1937.fm-tampermonkey-script/-/raw/main/script.user.js
// @updateURL    https://gitlab.com/avathar/dharma1937.fm-tampermonkey-script/-/raw/main/script.user.js
// ==/UserScript==

class RadioStation {
  constructor(url) {
    this.url = url;
  }

  async fetchStatus(timeout = 1000) {
    // timeout в миллисекундах
    const controller = new AbortController();
    const timeoutId = setTimeout(() => controller.abort(), timeout);

    try {
      const response = await fetch(this.url, { signal: controller.signal });
      clearTimeout(timeoutId);
      if (!response.ok) throw new Error(`Ошибка HTTP: ${response.status}`);
      let text = await response.text();
      text = text.replace('"title": -', '"title": "unknown"');
      return JSON.parse(text);
    } catch (error) {
      clearTimeout(timeoutId);
      if (error.name === 'AbortError') {
        //    console.error('Запрос был прерван из-за тайм-аута');
      } else {
        console.error(error.message);
      }
      return null;
    }
  }
}

class UIUpdater {
  constructor() {
    this.addStyles();
    this.maxListeners = 0;
    this.averageListeners = 0;
    this.medianListeners = 0;
    this.targetElement = null;
    this.oscilloscopeCanvas = null;
    this.audioCtx = null;
    this.radioBtns = [];
    this.radioBtnsContainer = null;
  }

  addStyles() {
    const style = document.createElement('style');
    style.textContent = `
            .custom-div {
                color: rgba(255, 255, 255, 0.7);
                background-color: rgba(0, 0, 0, 0.5);
                padding: 10px;
                border-radius: 5px;
                margin: 10px;
                text-wrap: wrap;
                position: absolute;
                top: 44px;
                text-align: center;
                width: 800px;
                font-size: 14px;
            }
            .added-canvas {
                position: fixed;
                z-index:100;
                bottom:70px;
                width: 100vw;
                height:20vh;
            }
            .added-canvas-style {
                width: 100%;
                height: 200px;
                position: fixed;
                z-index: 100;
                bottom: 70px;
            }
        `;
    document.head.appendChild(style);
  }

  setStringToDiv(text, divClass, parentDiv, isFirstChild) {
    let element = document.querySelector(`.${divClass}`);
    if (!element) {
      element = document.createElement('div');
      element.className = `${divClass}  rb-description custom-div`;
      if (isFirstChild) {
        parentDiv.insertBefore(element, parentDiv.firstChild);
      } else {
        parentDiv.appendChild(element);
      }
    }
    element.innerText = text;

    this.targetElement = element;
    this.createRadioButtons(element);
  }

  createRadioButtons(element) {
    //   if(!this.radioBtns.length) {
    const audioElement = document.getElementById('audio');

    // Добавляем радио-кнопки для переключения источника
    const sources = [
      {
        name: 'second',
        url: 'http://second.dharma1937.fm:7777/live',
        callback: () => audioElement.play(),
      },
      {
        name: 'third',
        url: 'http://third.dharma1937.fm:7777/live',
        callback: () => audioElement.play(),
      },
      { name: 'null', url: null, callback: () => {} },
    ];

    sources.forEach((source, index) => {
      const radioButton = document.createElement('input');
      radioButton.type = 'radio';
      radioButton.id = 'source' + index;
      radioButton.name = 'audioSource';
      radioButton.value = source.url;
      radioButton.checked = radioButton.value == audioElement.src;

      const label = document.createElement('label');
      label.htmlFor = 'source' + index;
      label.textContent = source.name;

      radioButton.addEventListener('change', function () {
        if (this.checked) {
          audioElement.src = this.value;
          source.callback();
        }
      });
      element.appendChild(radioButton);
      //  this.radioBtns.push(radioButton)
    });
    this.radioBtnsContainer = element.appendChild(
      document.createElement('div'),
    );
    //}
  }

  // Добавление кнопки и обработчика событий
  addDownloadButton(dbManager) {
    const downloadButton = document.createElement('button');
    downloadButton.textContent = 'Скачать уникальные названия';
    downloadButton.className = 'download-btn custom-button';
    const pattern = /^([\s\S]+?)(?:\s*[-–—]\s*|\s+)\1$/;
    function sortByFrequency(phrases) {
      // Подсчет количества повторений для каждой фразы
      const frequencyMap = phrases.reduce((acc, phrase) => {
        acc[phrase] = (acc[phrase] || 0) + 1;
        return acc;
      }, {});

      // Создание массива из уникальных фраз с их частотами
      const sortedPhrases = Object.keys(frequencyMap)
        .map((key) => ({ phrase: key, count: frequencyMap[key] }))
        // Сортировка по убыванию частоты
        .sort((a, b) => b.count - a.count);

      return sortedPhrases.map(
        (item) => `${item.phrase} (~${Math.round((item.count * 5) / 60)}min)`,
      );
    }
    function extractDuplicate(str) {
      const match = str.match(pattern);
      return match ? match[1].trim() : str;
    }
    downloadButton.addEventListener('click', async () => {
      const uniqueTitles = await dbManager.getUniqueTitles((filteredTitles) =>
        sortByFrequency(
          filteredTitles.map((originalString) =>
            extractDuplicate(
              this.partiallyDecodeString(
                originalString,
                'windows-1251', // предполагаемая неправильная кодировка
                (word) =>
                  word.includes('ê') ||
                  word.includes('ë') ||
                  word.includes('å') ||
                  word.includes('æ'), // простой критерий для определения неверных слов
              ),
            ),
          ),
        ),
      );
      this.downloadTitles(uniqueTitles);
    });
    this.radioBtnsContainer.appendChild(downloadButton); // Предполагается, что this.radioBtnsContainer - контейнер для радио-кнопок
  }

  // Функция для создания и скачивания файла
  downloadTitles(titles) {
    const blob = new Blob([titles.join('\n')], { type: 'text/plain' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'unique-titles.txt';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  }

  createOscilloscope(element) {
    // не работает из-за политики CORS сервера

    const audioElement = document.getElementById('audio');

    // Получаем аудио элемент и создаем аудио контекст
    const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    this.audioCtx = audioCtx;
    const analyser = audioCtx.createAnalyser();

    // Подключаем аудио элемент к анализатору
    const source = audioCtx.createMediaElementSource(audioElement);
    source.connect(analyser);
    analyser.connect(audioCtx.destination);

    // Настройки визуализации
    analyser.fftSize = 2048;
    const bufferLength = analyser.frequencyBinCount;
    const dataArray = new Uint8Array(bufferLength);

    // Создаем элемент для визуализации
    const canvas = document.createElement('canvas');
    this.oscilloscopeCanvas = canvas;
    canvas.width = '100vw';
    canvas.height = 100; // Высота осциллографа
    element.appendChild(canvas);

    const canvasCtx = canvas.getContext('2d');

    // Функция для отрисовки визуализации
    function draw() {
      requestAnimationFrame(draw);

      analyser.getByteTimeDomainData(dataArray);

      canvasCtx.fillStyle = 'rgb(200, 200, 200)';
      canvasCtx.fillRect(0, 0, canvas.width, canvas.height);

      canvasCtx.lineWidth = 2;
      canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

      canvasCtx.beginPath();

      const sliceWidth = (canvas.width * 1.0) / bufferLength;
      let x = 0;

      for (let i = 0; i < bufferLength; i++) {
        const v = dataArray[i] / 128.0;
        const y = (v * canvas.height) / 2;

        if (i === 0) {
          canvasCtx.moveTo(x, y);
        } else {
          canvasCtx.lineTo(x, y);
        }

        x += sliceWidth;
      }

      canvasCtx.lineTo(canvas.width, canvas.height / 2);
      canvasCtx.stroke();
    }

    // Запускаем визуализацию
    draw();
  }

  createTimeString(time) {
    const days = Math.floor(time / (1000 * 60 * 60 * 24));
    const hours = Math.floor((time % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
    // Формирование строки со временем
    const timeDifference = `${days} Дней, ${hours} Часов, ${minutes} Минут`;
    return timeDifference;
  }

  partiallyDecodeString(str, encoding, predicate) {
    const decoder = new TextDecoder(encoding);
    // Проверяем, подходит ли слово под критерий "неверной кодировки"
    if (predicate(str)) {
      // Декодируем, если слово считается неверно закодированным
      const encodedBytes = new Uint8Array(
        str.split('').map((c) => c.charCodeAt(0)),
      );
      return decoder.decode(encodedBytes);
    } else {
      // Оставляем как есть, если слово считается верно закодированным
      return str;
    }
  }

  createString(data) {
    const joinString = (str1, str2, summator) =>
      str1 !== str2
        ? str1 && str2
          ? str1 + summator + str2
          : str1 || str2
        : str1 || str2;
    const joinNum = (num1, num2, summator) =>
      `${num1 && num2 ? num1 + summator + num2 : num1 || num2}`;
    const joinSince = (time1, time2, uptime1, uptime2) =>
      uptime1 && uptime2
        ? `2: ${time1} ${this.createTimeString(
            uptime1,
          )}\n3: ${time2} ${this.createTimeString(uptime2)}`
        : uptime1
        ? `2: ${time1} ${this.createTimeString(uptime1)}`
        : uptime2`3: ${time2} ${this.createTimeString(uptime2)}`;
    const pattern = /^([\s\S]+?)(?:\s*[-–—]\s*|\s+)\1$/;

    data = JSON.parse(data);
    const listeners = `${data.listeners_sum} (${joinNum(
      data.listeners1,
      data.listeners2,
      '/',
    )})`;
    const originalString = joinString(data.title1, data.title2, ' ');

    let title = originalString
      ? this.partiallyDecodeString(
          originalString,
          'windows-1251', // предполагаемая неправильная кодировка
          (word) =>
            word.includes('ê') ||
            word.includes('ë') ||
            word.includes('å') ||
            word.includes('æ'), // простой критерий для определения неверных слов
        )
      : 'unknown';
    // функция убирает дублирование в названии (например мама мыла раму - мама мыла раму > мама мыла раму)
    function extractDuplicate(str) {
      const match = str.match(pattern);
      return match ? match[1].trim() : str;
    }
    title = extractDuplicate(title);
    let formatter = (text) => {
      try {
        const ft = new Intl.DateTimeFormat('ru', {
          weekday: 'short',
          year: 'numeric',
          month: 'short',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          seconds: 'numeric',
        });
        return ft.format(text);
      } catch {
        return 0;
      }
    };

    const d1 = new Date(data.stream_since1);
    const d2 = new Date(data.stream_since2);
    const fd1 = formatter(d1);
    const fd2 = formatter(d2);

    const since = joinSince(fd1, fd2, data.stream_uptime1, data.stream_uptime2);

    // const audioInfo = joinString(data.audio_info1, data.audio_info1, '\n');
    // const bitrate = joinNum(data.bitrate1, data.bitrate2, '...');
    // const channels = joinNum(data.channels1, data.channels2, '...');
    // const samplerate = joinNum(data.samplerate1, data.samplerate2, '...');
    const listeners_peak = `${data.listeners_peak_sum}(max:${
      this.maxListeners
    },avg:${this.averageListeners},med:${this.medianListeners})  (${joinNum(
      data.listeners_peak1,
      data.listeners_peak2,
      '/',
    )})`;
    // const info = `${audioInfo}\nOR: ${bitrate}/${channels}/${samplerate}`;
    return `${title}\nСлушает: ${listeners}\nМаксимум слушателей: ${listeners_peak}\nСтрим начат: \n${since}\n`;
  }
  //Аудиоинформация:\n${info}
  updateUI(data) {
    const circlesDiv = document.querySelector('.circles.circles-play');
    if (!circlesDiv) return;
    const text = this.createString(data);
    if (!this.oscilloscopeCanvas && this.targetElement && !this.audioCtx) {
      // this.createOscilloscope(this.targetElement)
    }
    this.setStringToDiv(text, 'added-div', circlesDiv, false);
    console.log(text);
  }
}

class ChartManager {
  constructor(dbManager) {
    this.dbManager = dbManager;
    this.chart = null;
    this.initChart();
  }

  setCanvasToDiv(canvas, divClass, parentDiv, isFirstChild) {
    let element = document.querySelector(`.${divClass}`);
    if (!element) {
      element = document.createElement('div');
      element.className = `${divClass} `;
      if (isFirstChild) {
        parentDiv.insertBefore(element, parentDiv.firstChild);
      } else {
        parentDiv.appendChild(element);
      }
    }
    element.appendChild(canvas);
  }

  async initChart() {
    const ctx = document.createElement('canvas').getContext('2d');
    ctx.canvas.classList.add('added-canvas-style'); // Применение класса к canvas

    const circlesDiv = document.querySelector('.circles.circles-play');
    if (!circlesDiv) return;
    this.setCanvasToDiv(ctx.canvas, 'added-canvas', circlesDiv, true);

    this.chart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            label: '',
            data: [],
            backgroundColor: 'rgba(0, 123, 255, 0.5)',
            borderColor: 'rgba(0, 123, 255, 1)',
            borderWidth: 1,
            cubicInterpolationMode: 'monotone',
            tension: 0.4,
            fill: true,
            spanGaps: true,
            stepped: false,
          },
        ],
      },
      options: {
        maintainAspectRatio: false, // Отключает поддержку соотношения сторон
        responsive: true, // Подстраивается под размер контейнера
        spanGaps: true, // enable for all datasets

        layout: {
          padding: 20,
        },
        plugins: {
          legend: {
            display: false, // Скрытие легенды
          },
        },
        scales: {
          x: {
            display: false, // Отключаем отображение оси X
          },
          y: {
            beginAtZero: true,
          },
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            title: (tooltipItems, data) => {
              // Возвращаем подпись для оси X при наведении
              return data.labels[tooltipItems[0].index];
            },
          },
        },
        hover: {
          mode: 'nearest',
          intersect: true,
        },
      },
    });
  }
  decimateArray(data, newSize) {
    const factor = Math.floor(data.length / newSize);
    return data.filter((_, index) => index % factor === 0);
  }
  averageDecimateArray(data, newSize) {
    const factor = Math.floor(data.length / newSize);
    const result = [];

    for (let i = 0; i < data.length; i += factor) {
      let sum = 0;
      let count = 0;

      for (let j = i; j < i + factor && j < data.length; j++) {
        sum += data[j];
        count++;
      }

      result.push(sum / count);
    }

    return result;
  }
  async updateChart(newSize = 1500) {
    const data = await this.dbManager.getAllData();

    // Децимация данных и меток
    const decimatedData = this.decimateArray(
      data.map((item) => item.listeners_sum),
      newSize,
    );
    const decimatedLabels = this.decimateArray(
      data.map((item) => new Date(item.now_date).toLocaleString()),
      newSize,
    );

    // Обновление данных графика
    this.chart.data.labels = decimatedLabels;
    this.chart.data.datasets.forEach((dataset, index) => {
      dataset.data = decimatedData;
    });

    // Обновление графика
    this.chart.update();
  }

  //   async updateChart() {
  //     const data = await this.dbManager.getAllData();
  //     this.chart.data.labels = data.map((item) =>
  //       new Date(item.now_date).toLocaleString(),
  //     );
  //     this.chart.data.datasets.forEach((dataset) => {
  //       dataset.data = data.map((item) => item.listeners_sum);
  //     });
  //     this.chart.update();
  //   }
}

class DatabaseManager {
  constructor() {
    this.dbName = 'RadioStationDB';
    this.storeName = 'RadioData';
    this.initDB();
  }

  async initDB() {
    return new Promise((resolve, reject) => {
      const request = indexedDB.open(this.dbName, 1);
      request.onupgradeneeded = (event) => {
        const db = event.target.result;
        db.createObjectStore(this.storeName, {
          keyPath: 'id',
          autoIncrement: true,
        });
      };
      request.onsuccess = () => resolve(request.result);
      request.onerror = (event) => reject(event.target.error);
    });
  }
  // Функция для поиска уникальных названий треков
  async getUniqueTitles(callback) {
    const allData = await this.getAllData();
    const titles = allData.flatMap((data) => [data.title1 /* , data.title2 */]);
    const filteredTitles = titles.filter(
      (title) => title && title !== 'unknown',
    );

    return callback(filteredTitles);
  }
  async saveData(data) {
    const db = await this.initDB();
    const transaction = db.transaction(this.storeName, 'readwrite');
    const store = transaction.objectStore(this.storeName);
    store.add(data);
  }

  async getAllData() {
    const db = await this.initDB();
    return new Promise((resolve, reject) => {
      const transaction = db.transaction(this.storeName, 'readonly');
      const store = transaction.objectStore(this.storeName);
      const request = store.getAll();
      request.onsuccess = () => resolve(request.result);
      request.onerror = () => reject(request.error);
    });
  }
}

(async function () {
  ('use strict');
  // Подключение библиотеки Chart.js
  const chartScript = document.createElement('script');
  chartScript.src = 'https://cdn.jsdelivr.net/npm/chart.js';
  document.head.appendChild(chartScript);
  const station1 = new RadioStation(
    'http://second.dharma1937.fm:7777/status-json.xsl',
  );
  const station2 = new RadioStation(
    'http://third.dharma1937.fm:7777/status-json.xsl',
  );
  const uiUpdater = new UIUpdater();
  const dbManager = new DatabaseManager();
  let chartManager = null;

  chartScript.onload = async () => {
    chartManager = new ChartManager(dbManager);

    await chartManager.updateChart();
  };

  async function getStatusAndUpdateUI() {
    const data1 = await station1.fetchStatus();
    const data2 = await station2.fetchStatus();

    if (!data1 && !data2) {
      console.error('Не удалось получить данные ни от одной или станций');
      return;
    }

    const listenersSum =
      (parseInt(data1?.icestats?.source?.listeners) || 0) +
      (parseInt(data2?.icestats?.source?.listeners) || 0);
    const listenersPeakSum =
      (parseInt(data1?.icestats?.source?.listener_peak) || 0) +
      (parseInt(data2?.icestats?.source?.listener_peak) || 0);
    const now = new Date();
    const d1 = new Date(data1?.icestats?.source?.stream_start_iso8601);
    const d2 = new Date(data2?.icestats?.source?.stream_start_iso8601);

    // Подготовка и объединение данных для отображения и сохранения
    const combinedData = {
      title1: data1?.icestats?.source?.title || null,
      title2: data2?.icestats?.source?.title || null,
      listeners_sum: listenersSum || 0,
      listeners1: parseInt(data1?.icestats?.source?.listeners) || 0,
      listeners2: parseInt(data2?.icestats?.source?.listeners) || 0,
      listeners_peak_sum: listenersPeakSum || 0,
      listeners_peak1: parseInt(data1?.icestats?.source?.listener_peak) || 0,
      listeners_peak2: parseInt(data2?.icestats?.source?.listener_peak) || 0,
      stream_since1: data1?.icestats?.source?.stream_start_iso8601 || null,
      stream_since2: data2?.icestats?.source?.stream_start_iso8601 || null,
      stream_uptime1: now.getTime() - d1.getTime() || null,
      stream_uptime2: now.getTime() - d2.getTime() || null,
      audio_info1: data1?.icestats?.source?.audio_info || null,
      audio_info2: data2?.icestats?.source?.audio_info || null,
      bitrate1: data1?.icestats?.source?.bitrate || null,
      bitrate2: data2?.icestats?.source?.bitrate || null,
      channels1: data1?.icestats?.source?.channels || null,
      channels2: data2?.icestats?.source?.channels || null,
      samplerate1: data1?.icestats?.source?.samplerate || null,
      samplerate2: data2?.icestats?.source?.samplerate || null,
      now_date: now,
    };

    uiUpdater.updateUI(JSON.stringify(combinedData, null, 2));
    // Инициализация кнопки скачивания
    uiUpdater.addDownloadButton(dbManager);
    const filtered = (arr) =>
      arr.filter((item) => typeof item === 'number' && !isNaN(item));

    function calculateAverage(arr) {
      if (!Array.isArray(arr) || arr.length === 0) {
        return 0;
      }
      if (arr.length === 0) {
        return 0;
      }
      let sum = arr.reduce((acc, val) => acc + val, 0);
      return Math.floor(sum / arr.length);
    }

    function calculateMedian(arr) {
      if (!Array.isArray(arr) || arr.length === 0) {
        return 0;
      }
      if (arr.length === 0) {
        return 0;
      }
      arr.sort((a, b) => a - b);
      const mid = arr.length / 2;
      return mid % 1 ? arr[Math.floor(mid)] : (arr[mid - 1] + arr[mid]) / 2;
    }

    await dbManager.saveData(combinedData);
    const allData = await dbManager.getAllData();
    const allListeners = allData.map((data) => data.listeners_sum);
    const filteredListeners = filtered(allListeners);
    const averageListeners = calculateAverage(filteredListeners);
    const medianListeners = calculateMedian(filteredListeners);
    const maxListeners = Math.max(...filteredListeners);
    uiUpdater.maxListeners = maxListeners;
    uiUpdater.averageListeners = averageListeners;
    uiUpdater.medianListeners = medianListeners;

    if (chartManager) {
      await chartManager.updateChart(); // Обновление графика с новыми данными
    }
  }

  setInterval(getStatusAndUpdateUI, 5000);
})();
