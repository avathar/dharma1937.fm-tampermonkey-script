#!/bin/bash

# Функция для получения текущего времени и форматирования его в нужный формат
get_filename() {
    echo $(date +"%Y-%m-%d-%H-%M-%S")-dharma1937fm.mp3
}

# Функция для обработки сигнала прерывания (SIGINT)
cleanup() {
    echo "Прерывание скрипта. Запуск mp3val..."
    mp3val "$FILENAME" -f -t
    exit
}

# Установка обработчика сигнала SIGINT
trap cleanup SIGINT

# Бесконечный цикл для перезапуска записи
while true; do
    # Генерация имени файла
    FILENAME=$(get_filename)

    # Выполнение команды wget для скачивания аудиопотока
    wget -O "$FILENAME" http://second.dharma1937.fm:7777/live
    mp3val "$FILENAME" -f -t
    # Краткая пауза перед следующей попыткой записи (на случай, если поток не доступен)
    sleep 5
done
