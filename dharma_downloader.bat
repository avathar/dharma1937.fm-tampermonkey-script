function Get-FileName {
    return (Get-Date -Format "yyyy-MM-dd-HH-mm-ss") + "-dharma1937fm.mp3"
}

$cleanup = {
    Write-Host "Прерывание скрипта. Запуск mp3val..."
    Start-Process "mp3val" -ArgumentList "$global:FILENAME", "-f", "-t" -NoNewWindow -Wait
}

trap {
    & $cleanup
    break
}

while ($true) {
    $global:FILENAME = Get-FileName

    Invoke-WebRequest -Uri "http://second.dharma1937.fm:7777/live" -OutFile $global:FILENAME
    Start-Process "mp3val" -ArgumentList "$global:FILENAME", "-f", "-t" -NoNewWindow -Wait

    Start-Sleep -Seconds 5
}
